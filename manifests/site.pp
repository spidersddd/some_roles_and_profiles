#include profiles::app::puppet::server

File {
  backup  => false,
}

if $trusted['extensions']['pp_role'] {
  include "role::${trusted['extensions']['pp_role']}"
}

node 'bolt_test_box.tncklein.info' {
  include roles::bolt::server
}

node 'squid.tncklein.info' {
  include roles::squid::server
}

node 'puppetserver.tncklein.info' {
  include roles::puppet::server
}

node default {
  include baseline_profiles::baseline
}
