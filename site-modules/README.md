# Puppet Example Roles and Profiles

These directories are layout of Roles practice.  They have been constructed to represent current best practice and to support multiple operating systems.

It SHOULD go without saying that everything should pass linting/validation, but
we're gonna go ahead and say that anyway.

## Example requirements

These examples have been constructed with the following requirements:

| Supported OS | Product Role | State |
|--------------------------|---------------------|-----|
| CentOS 7 | role::config_mgmt::cert_authority | Complete |
| CentOS 7 | role::config_mgmt::database_server | Complete |
| CentOS 7 | role::config_mgmt::databaseapi_server | Complete |


  - Modeling should support three operating systems
    - CentOS (6,7)
    - Debian (10)
  - One product should be represented
    - Config_mgmt (WIP)
      - Linux systems hosting the product
  - Support services
    - While products usually do not share services, support services are used by many products and teams.
    - Example of monitoring service 'role::sup\_svc::monitoring::server'
    - Example of Puppet Master 'role::sup\_svc::storage::server'
