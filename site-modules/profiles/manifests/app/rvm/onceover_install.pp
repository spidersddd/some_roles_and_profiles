# profile to install onceover
class profiles::app::rvm::onceover_install (
) {
  include profiles::app::rvm::install_system_ruby
  $ruby_version = $profiles::app::rvm::install_system_ruby::ruby_version

  package { 'hiera-eyaml':
    ensure   => '3.2.1',
    provider => 'gem',
  }

  package { 'onceover':
    ensure   => present,
    provider => 'gem',
    require  => [ Class["profiles::app::rvm::install_system_ruby"],
      Package['hiera-eyaml'], ]
  }
}
