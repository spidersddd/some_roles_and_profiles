class profiles::app::rvm::install_system_ruby (
  String $ruby_version = '2.6.3',
) {
  package { 'ruby':
    ensure => absent,
  }

  class { '::rvm':
    key_server   => 'hkp://pool.sks-keyservers.net',
    gnupg_key_id => '39499BDB',
    require      => Package['ruby'],
  }
  rvm_system_ruby {
    "ruby-${ruby_version}":
      ensure      => 'present',
      default_use => true,
      build_opts  => ['--binary'];
  }
}
